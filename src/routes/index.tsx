import React from 'react'
import { Suspense } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { Dashboard } from '../pages/dashboard'
import { Repository } from '../pages/repository'

export const Routes: React.FC = () => {
	return (
		<BrowserRouter>
			<Suspense fallback={'Loading...'}>
				<Switch>
					<Route component={Dashboard} path="/" exact />
					<Route component={Repository} path="/repositories/:repository+" />
				</Switch>
			</Suspense>
		</BrowserRouter>
	)
}
