import React from 'react'
import { Routes } from './routes'
import { GlobalStyles } from './styles/global'

export const App = () => {
	return (
		<>
			<GlobalStyles />
			<Routes />
		</>
	)
}
