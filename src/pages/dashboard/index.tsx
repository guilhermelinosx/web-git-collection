import React from 'react'
import { useState, useRef, useEffect } from 'react'
import { Title, Form, Repos, Error } from './styles'
import { FiChevronRight } from 'react-icons/fi'
import { Link } from 'react-router-dom'
import logo from '../../assets/logo.svg'
import { Api } from '../../services/api'

interface GithubRepository {
	full_name: string
	description: string
	owner: {
		login: string
		avatar_url: string
	}
}

export const Dashboard = () => {
	const [repos, setRepos] = useState<GithubRepository[]>(() => {
		const storageRepos = localStorage.getItem('@GitCollection:repositories')
		if (storageRepos != null) {
			return JSON.parse(storageRepos)
		}
		return []
	})
	const [newRepo, setNewRepo] = useState('')
	const [inputError, setInputError] = useState('')

	const formEl = useRef<HTMLFormElement | null>(null)

	useEffect(() => {
		localStorage.setItem('@GitCollection:repositories', JSON.stringify(repos))
	}, [repos])

	function handleInputChange(event: React.ChangeEvent<HTMLInputElement>): void {
		setNewRepo(event.target.value)
	}

	async function handleAddRepo(event: React.FormEvent<HTMLFormElement>): Promise<void> {
		event.preventDefault()

		if (newRepo.length === 0) {
			return setInputError('Enter username/repository')
		}

		try {
			const response = await Api.get<GithubRepository>(`repos/${newRepo}`)
			const repository = response.data

			setRepos([...repos, repository])
			formEl.current?.reset()
			setNewRepo('')
			setInputError('')
		} catch {
			setInputError('Repositório não encontrado no Github')
		}
	}

	return (
		<>
			<img src={logo} alt="Logo GitCollection" />
			<Title>
				Catalog of <br />
				repositories of <br />
				GitHub
			</Title>
			<Form ref={formEl} hasError={Boolean(inputError)} onSubmit={handleAddRepo}>
				<input placeholder="username/repository" onChange={handleInputChange} />
				<button type="submit">Search</button>
			</Form>

			{inputError.length > 0 && <Error>{inputError}</Error>}

			<Repos>
				{repos.map((repository) => (
					<Link to={`repositories/${repository.full_name}`} key={repository.full_name}>
						<img src={repository.owner.avatar_url} alt={repository.owner.login} />
						<div>
							<strong>{repository.full_name}</strong>
							<p>{repository.description}</p>
						</div>
						<FiChevronRight size={20} />
					</Link>
				))}
			</Repos>
		</>
	)
}
