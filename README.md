# <div align="center"> Web Git Collection </div>

<div align="center">
</br>
<p> Web application developed for the purpose of listing and pinning your favorite Github repositories. </p>
</div>
</br>

![image 1](/.github/image2.png 'Image 1')

</br>

## Technologies used in the project

- Typescript
- ReactJs
- Styled Components
- Axios

## How to run the project

- Clone this repository

```shell
git clone https://github.com/guilhermelinosx/web-git-collection.git
```

</br>

- Start the Application in Development

```shell
yarn install
yarn dev
```

</br>

- To stop the Application click CTRL+C in your terminal
